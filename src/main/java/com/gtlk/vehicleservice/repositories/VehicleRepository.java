package com.gtlk.vehicleservice.repositories;

import com.gtlk.vehicleservice.domain.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface VehicleRepository extends MongoRepository<Vehicle, String> {
}
