package com.gtlk.vehicleservice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
@SuperBuilder
@NoArgsConstructor
public class Vehicle {
    private String id;
    private String make;
    private String model;
    private Double cost;
    private Integer seats;
}
